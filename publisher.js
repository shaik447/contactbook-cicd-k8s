var mq = require('amqplib/callback_api')
//publisher
mq.sendMessageToQ=function(message){
    //console.log(message);
    if(process.env.mqurl == undefined) {
        console.log('RabbitMq config not set');
        return;
    }
    mq.connect(process.env.mqurl,(err,connection)=>{
        if (err) console.log('Connection failed to mq '+err.message);
        connection.createChannel((err,channel)=>{
            const queueName='mailingQ'
            const msg=JSON.stringify(message);
            channel.assertQueue(queueName,{durable:false});
            channel.sendToQueue(queueName,Buffer.from(msg));
            console.log('[x] Sent %s', msg);
            
        });
    });
};

//subscriber
mq.receiveMessagefromQ=function(queueName){
    mq.connect(process.env.mqurl,(err,connection)=>{
        if (err) console.log('Connection failed to mq ');
        connection.createChannel((err,channel)=>{
            if (err) console.log('Channel creation failed');
            const queueName='mailingQ'
            channel.assertQueue(queueName,{durable:false});
            channel.consume(queueName,(msg)=>{
                console.log('[x] Received %s', msg.content.toString());

            },{noAck:true});
            
        });
    });
}


module.exports = mq;