import pika
import os
import json

# print('message Q url is: '+os.environ['mqurl'])
credentials= pika.PlainCredentials('guest','guest')
connection = pika.BlockingConnection(pika.ConnectionParameters(os.environ['mqurl'],5672,"/",credentials))
channel = connection.channel()

channel.queue_declare(queue='mailingQ')
def callback(ch,method,properties,body):
    # print(body)
    jstring=body.decode('utf-8')
    receivedMessage=json.loads(jstring)
    name=receivedMessage["name"]
    email=receivedMessage["email"]
    print('Sending email to '+name+" at "+email+"....")

channel.basic_consume(queue='mailingQ',auto_ack=False,on_message_callback=callback)

print('[*] Waiting for messages/ To exit press Ctrl+C')
channel.start_consuming()
