var express = require('express');
var router = express.Router();
const Contact=require('../models/DbSchema').Contact;
const publisher=require('../publisher');

/* GET home page. */
router.get('/', function(req, res, next) {
  Contact.find((err,contacts)=>{
    if(err) res.status(404).json('No Contacts');
    //console.log(contacts);
    //publisher.receiveMessagefromQ("mailingQ");
    res.render('index', {contacts:contacts});

  });
  
});

router.post('/',(req,res)=>{
  var contact= new Contact({
    name: req.body.name,
    email: req.body.email,
    phone: req.body.phone
  });
  //console.log(contact);
  contact.save((err,contact)=>{
    if(err) return res.status(500).json("Could not save the contact");
    res.send(contact);
    publisher.sendMessageToQ(contact);

  });
  

});

module.exports = router;
