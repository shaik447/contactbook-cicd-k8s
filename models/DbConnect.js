const mongoose=require('mongoose');

async function connectDB() {
    try {
        const DB= await mongoose.connect(process.env.DbUrl,{useNewUrlParser:true});
        console.log('Connection to database succeeded');
        return DB;
    } catch (error) {
        console.log('Connection to database failed '+error.message);
    }
}

exports.connectDB = connectDB;