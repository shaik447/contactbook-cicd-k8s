const mongoose= require('mongoose');
var mongoschema={};

const contactSchema= new mongoose.Schema({
    name: {type:String,minlength:3},
    email: {type:String,minlength:3},
    phone: {type:String,minlength:3},
});

mongoschema.Contact=mongoose.model('Contact',contactSchema);

module.exports = mongoschema;