$(document).ready(function(){

    $('#btnsavecontact').click(function () {
        var contact={
            name: $('#txtname').val(),
            email: $('#txtemail').val(),
            phone: $('#txtphone').val()
        };
        //console.log(JSON.stringify(contact));
        $.ajax({
            type: 'POST',
            url: "/",
            data: JSON.stringify(contact),
            success: function (data) {
                console.log(data);
                location.reload();
            },
            error: function (err) {
              //$('#myalert').removeClass('.d-none');  
              $('#myalert').fadeIn(); 
            },
            contentType: "application/json",
            dataType: 'json'
        });

        
    })
});